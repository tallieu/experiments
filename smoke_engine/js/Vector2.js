'use strict';

if( limarc == undefined )
{
	var limarc = {};
}

limarc.toRadians = function( degrees )
{
	return degrees * Math.PI / 180;
};

limarc.Vector2 =
{
	create: function( x, y )
	{
		var o = Object.create( this );
		o.x = x;
		o.y = y;
		return o;
	},
	length: function()
	{
		return Math.sqrt( this.x * this.x + this.y * this.y );
	},
	angle: function()
	{
		return Math.atan2( this.y, this.x );
	},
	lerp: function( v, factor )
	{
		return v.sub( this ).mul_f( factor ).add( this );
	},
	dot: function( v )
	{
		return ( this.x * v.x ) + ( this.y * v.y );
	},
	normalized: function()
	{
		var length = this.length();
		return limarc.Vector2.create( this.x / length, this.y / length );
	},
	cross: function( v )
	{
		return ( this.x * v.y ) - ( this.y * v.x );
	},
	rotate: function( angle )
	{
		var rad = limarc.toRadians( angle );
		var cos = Math.cos( rad );
		var sin = Math.sin( rad );

		return limarc.Vector2.create( this.x * cos - this.y * sin, this.x * sin + this.y * cos );
	},
	add: function( v )
	{
		return limarc.Vector2.create( this.x + v.x, this.y + v.y );
	},
	add_f: function( v )
	{
		return limarc.Vector2.create( this.x + v, this.y + v );
	},
	sub: function( v )
	{
		return limarc.Vector2.create( this.x - v.x, this.y - v.y );
	},
	sub_f: function( v )
	{
		return limarc.Vector2.create( this.x - v, this.y - v );
	},
	mul: function( v )
	{
		return limarc.Vector2.create( this.x * v.x, this.y * v.y );
	},
	mul_f: function( v )
	{
		return limarc.Vector2.create( this.x * v, this.y * v );
	},
	div: function( v )
	{
		return limarc.Vector2.create( this.x / v.x, this.y / v.y );
	},
	div_f: function( v )
	{
		return limarc.Vector2.create( this.x / v, this.y / v );
	},
	abs: function()
	{
		return limarc.Vector2.create( Math.abs( this.x ), Math.abs( this.y ) );
	},
	equals: function( v )
	{
		return ( this.x === v.x && this.y === v.y );
	}
};