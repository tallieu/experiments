if( limarc == undefined )
{
	var limarc = {};
}

limarc.floatBetween = function( max, min )
{
	return ( Math.random() * ( min - max ) + max ).toFixed( 4 );
};

limarc.intBetween = function( max, min )
{
	return ( Math.random() * ( min - max ) + max );
};

limarc.intBetweenPosNeg = function( max, min )
{
	var num = limarc.intBetween( max, min );
	num *= Math.floor( Math.random() * 2 ) == 1 ? 1 : -1;
	return num;
};

limarc.ParticleEmitter =
{
	create: function( settings )
	{
		var o = Object.create( this );
		
		o.settings = {
			amount: 50,
			maxVelocity: 2,
			minVelocity: 0.01,
			maxLifespan: 300,
			minLifespan: 50,
			angle: 10,
			minAlpha: 0.01,
			maxAlpha: 0.1,
			minScale: 1,
			maxScale: 3,
			turbulenceX: 80,
			turbulenceY: 80,
			texture: "particle.png"
		};
		
		$.extend( o.settings, settings );
		
		o.container = new PIXI.DisplayObjectContainer();
		o.texture = PIXI.Texture.fromImage( o.settings.texture );
		
		o.displacementTexture = PIXI.Texture.fromImage( "noise.png" );
		o.displacementFilter = new PIXI.DisplacementFilter( o.displacementTexture );
		
		o.displacementFilter.scale.x = o.settings.turbulenceX;
		o.displacementFilter.scale.y = o.settings.turbulenceY;
	
		o.position = limarc.Vector2.create( 410, 340 );
		o.direction = limarc.Vector2.create( 0.5, -1 );
		
		o.particles = [];

		o.container.filters = [ o.displacementFilter ];
		o.container.filterArea = new PIXI.Rectangle( 0, 0, 2000, 2000 );
		
		return o;
	},
	start: function()
	{
		for( var i = 0; i < this.settings.amount; i++ )
		{
			this.particles.push( limarc.Particle.create( this ) );
		}
	},
	stop: function()
	{
	},
	update: function()
	{
		for( var k in this.particles )
		{
			this.particles[ k ].update();
		}
	}
};

limarc.Particle =
{
	create: function( particleEmitter )
	{
		var o = Object.create( this );
		
		o.particleEmitter = particleEmitter;
		o.container = new PIXI.Sprite( o.particleEmitter.texture );
		o.container.anchor.x = 0.5;
		o.container.anchor.y = 0.5;
		
		o.reset();
		
		o.particleEmitter.container.addChild( o.container );
		return o;
	},
	reset: function()
	{
		this.position = this.particleEmitter.position;

		this.lifespan = limarc.intBetween( this.particleEmitter.settings.minLifespan, this.particleEmitter.settings.maxLifespan );
		this.half_lifespan = this.lifespan / 2;

		this.speed = limarc.floatBetween( this.particleEmitter.settings.minVelocity, this.particleEmitter.settings.maxVelocity );

		this.target_alpha = limarc.floatBetween( this.particleEmitter.settings.minAlpha, this.particleEmitter.settings.maxAlpha );
		this.current_alpha = 0;

		this.target_scale = limarc.floatBetween( this.particleEmitter.settings.minScale, this.particleEmitter.settings.maxScale );

		if( this.particleEmitter.autoScale )
		{
			this.current_scale = 0;
			this.scale_factor = this.target_scale / this.half_lifespan;
		}
		else
		{
			this.current_scale = this.target_scale;
			this.scale_factor = 0;
		}

		this.angle = limarc.intBetweenPosNeg( -( this.particleEmitter.settings.angle / 2 ), ( this.particleEmitter.settings.angle / 2 ) );

		this.direction = this.particleEmitter.direction.rotate( this.angle );
		this.velocity = this.direction.mul_f( this.speed );

		this.alpha_factor = this.target_alpha / this.half_lifespan;
	},
	update: function()
	{
		this.lifespan = this.lifespan - 1;
		
		if( this.lifespan < 0 )
		{
			this.reset();
		}
		else if( this.lifespan > this.half_lifespan )
		{
			this.current_scale = this.current_scale + this.scale_factor;
			this.current_alpha += this.alpha_factor;
		}
		else
		{
			this.current_alpha -= this.alpha_factor;
		}

		this.position = this.position.add( this.velocity );

		this.container.alpha = this.current_alpha;

		this.container.scale.x = this.current_scale;
		this.container.scale.y = this.current_scale;

		this.container.position.x = this.position.x;
		this.container.position.y = this.position.y;
	}
}