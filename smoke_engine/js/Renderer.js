'use strict';

if( limarc == undefined )
{
	var limarc = {};
}

requestAnimFrame = (function() {
return window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame ||
function(/* function FrameRequestCallback */ callback, /* DOMElement Element */ element) {
window.setTimeout(callback, 1000/60);
}; })();

limarc.Renderer =
{
	create: function()
	{
		var o = Object.create( this );
		
		o.$window = $( window );
		
		o.width = o.$window.width();
		o.height = o.$window.height();
		
		o.stage = new PIXI.Stage( 0x000000 );
		o.renderer = PIXI.autoDetectRenderer( o.width, o.height, null, true, true );
		
		document.body.appendChild( o.renderer.view );
		
		o.drawables = [];
		
		o.render();
		
		return o;
	},
	render: function()
	{
		var that = this;
		
		requestAnimFrame( function()
		{
			that.render();
		} );
		
		for( var k in this.drawables )
		{
			this.drawables[ k ].update();
		}
		
		this.renderer.render( this.stage );
	},
	add: function( drawable )
	{
		this.drawables.push( drawable );
		this.stage.addChild( drawable.container );
	}
};