$( document ).ready( function()
{
	// create an new instance of a pixi stage
	var renderer = limarc.Renderer.create();

	particleEmitter = limarc.ParticleEmitter.create();

	particleEmitter2 = limarc.ParticleEmitter.create( {
		amount: 100,
		maxVelocity: 5,
		minVelocity: 4,
		minAlpha: 0.02,
		maxAlpha: 0.5,
		maxScale: 4,
		minScale: 3,
		minLifespan: 50,
		maxLifespan: 150,
		angle: 120,
		turbulenceX: 30,
		turbulenceY: 30,
		autoScale: true
	} );

	particleEmitter2.position = limarc.Vector2.create( 50, 500 );
	particleEmitter2.direction = limarc.Vector2.create( 1, -1 );

	renderer.add( particleEmitter );
	//renderer.add( particleEmitter2 );

	particleEmitter.start();
	particleEmitter2.start();
} );